package com.cnss.demoData.rest.util;

import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(HttpStatus.BAD_REQUEST)

public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }
}
