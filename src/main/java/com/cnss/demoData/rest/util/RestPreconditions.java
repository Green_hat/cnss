package com.cnss.demoData.rest.util;

import org.springframework.boot.context.config.ResourceNotFoundException;

public class RestPreconditions {
    public static <T> T checkFound(T resource) throws Exception {
        if (resource == null) {
            throw new Exception();
        }
        return resource;
    }
}