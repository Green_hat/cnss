package com.cnss.demoData.rest;

import com.cnss.demoData.model.Assure;
import com.cnss.demoData.repositories.AssureRepository;
import com.cnss.demoData.rest.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/assure")
public class AssureController {
    @Autowired
    private AssureRepository assureRepository;


    @RequestMapping(method = RequestMethod.GET)
    public List<Assure> findAll() {
        return assureRepository.findAll();
    }

//  @Secured("ROLE_USER")
    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET)
    public Assure findOne(@PathVariable("id") Long id) {
        return assureRepository.findOne( id );
    }

    @RequestMapping( method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Assure create(@RequestBody  Assure assure){
        return  assureRepository.save( assure );
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable Long id,@RequestBody  Assure assure){
        if(assure.getId()!=id) throw new BadRequestException("entity with id does not exist");
        assureRepository.save( assure );
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id){
          assureRepository.delete( id );
    }
}