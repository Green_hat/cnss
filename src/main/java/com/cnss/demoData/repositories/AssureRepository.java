package com.cnss.demoData.repositories;
import com.cnss.demoData.model.Assure;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssureRepository extends JpaRepository<Assure,Long>{
    List<Assure> findByMatriculeStartingWith(String mat);
}
