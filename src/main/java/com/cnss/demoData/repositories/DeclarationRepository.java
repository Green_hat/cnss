package com.cnss.demoData.repositories;
import com.cnss.demoData.model.Assure;
import com.cnss.demoData.model.Declaration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeclarationRepository extends JpaRepository<Declaration,Long>{
}
