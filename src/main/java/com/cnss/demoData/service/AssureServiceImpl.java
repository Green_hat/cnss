package com.cnss.demoData.service;

import com.cnss.demoData.model.Assure;
import com.cnss.demoData.repositories.AssureRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssureServiceImpl implements AssureService {

    private final AssureRepository assureRepository;

    public AssureServiceImpl(AssureRepository assureRepository) {
        this.assureRepository = assureRepository;
    }

    @Override
    public List<Assure> findAll() {
        return assureRepository.findAll();
    }

    @Override
    public Assure findOne(Long id) {
        return assureRepository.findOne(id);
    }
}
