package com.cnss.demoData.service;

import com.cnss.demoData.model.Assure;

import java.util.List;


public interface AssureService {

    List<Assure> findAll();

    Assure findOne(Long id);
}
