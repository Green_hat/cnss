package com.cnss.demoData.aop;

import com.cnss.demoData.model.Assure;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Logging {

    @Pointcut("execution(* com.cnss.demoData.rest.*.*(..))")
    private void allHandlers(){}


    @Before("allHandlers()")
    public void beforeAdvice(){
        System.out.println("Action a executer.");
    }
    @After("allHandlers()")
    public void afterAdvice(){
        System.out.println("Action Executée");
    }
    @AfterReturning(pointcut = "allHandlers()", returning = "ret")
    public void afterret(Object ret) {
        System.out.println(ret.toString());

    }

}
