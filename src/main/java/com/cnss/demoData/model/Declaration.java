package com.cnss.demoData.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Declaration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long valeur;
    @Temporal(TemporalType.DATE)
    private Date date;
    @ManyToOne
    private Assure assure;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValeur() {
        return valeur;
    }

    public void setValeur(Long valeur) {
        this.valeur = valeur;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
