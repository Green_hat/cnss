package com.cnss.demoData.model;



import javax.persistence.*;

import java.util.Date;

@Entity
public class Assure {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nom;

    private String matricule;

    private Long salaire;

    public Long getSalaire() {
        return salaire;
    }

    public void setSalaire(Long salaire) {
        this.salaire = salaire;
    }

    @Temporal(TemporalType.DATE)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }


    @Override
    public String toString() {
        return "Assure{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", matricule='" + matricule + '\'' +
                '}';
    }
}
